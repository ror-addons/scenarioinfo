scenarioInfo = {}
LibGUI = LibStub("LibGUI")

local F
scenarioInfo.Settings = nil

scenarioInfo.allfields = {
    "Career",
    "Name",
    "Rank",
    "SoloKills",
    "GroupKills",
    "Renown",
    "DeathBlows",
    "Deaths",
    "DamageDealt",
    "HealingDealt",
    "Experience",
}

scenarioInfo.columndefs = {}
scenarioInfo.columndefs["kills"] = { slice = "multi-skulls", order = 2, width = 18, height = 16, datafield = "groupkills"}
scenarioInfo.columndefs["deaths"] = { slice = "tombstone", order = 3, width = 13, height = 16, datafield = "deaths"}
scenarioInfo.columndefs["renown"] = { slice = "crown", order = 4, width = 14, height = 16, datafield = "renown"}
scenarioInfo.columndefs["db"] = { slice = "sword-skull", order = 5, width = 10, height = 16, datafield = "deathblows"}
scenarioInfo.columndefs["damage"] = { slice = "axe", order = 6, width = 16, height = 16, datafield = "damagedealt"}
scenarioInfo.columndefs["heal"] = { slice = "health", order = 7, width = 14, height = 14, datafield = "healingdealt"}
scenarioInfo.columndefs["solokills"] = { slice = "single-skull", order = 8, width = 14, height = 16, datafield = "solokills"}
scenarioInfo.columndefs["rank"] = { slice = "rank", order = 10, width = 10, height = 16, datafield = "rank"}
scenarioInfo.columndefs["experience"] = { slice = "xp", order = 11, width = 14, height = 16, datafield = "experience"}    

scenarioInfo.maxRows = 5
local DelayTime = 0
local REFRESH_DELAY = 2
local REFRESH_DELAY_SCOREBOARD = 1

function scenarioInfo.Initialize()

  if (not scenarioInfo.Settings) then
    scenarioInfo.Settings = {
      x = 800,
      y = 500,
      }
  end

  if (not scenarioInfo.Settings.column1) then
      scenarioInfo.Settings.column1 = "damage"
      scenarioInfo.Settings.column2 = "heal"
  end
  
  if (F) then
    LayoutEditor.UnregisterWindow(F.name)
    F:Destroy()
  end  
  
  F = LibGUI("frame", "scenarioInfo")
  F:Resize(300,30+19*scenarioInfo.maxRows)
  
  for num=1, scenarioInfo.maxRows do
    local row = "scenarioInfoPlayerRow"..num
    if CreateWindowFromTemplate(row, "ScenarioSummaryPlayerRow", F.name) then  
        WindowSetShowing( row, false )
        WindowClearAnchors(row)
        WindowAddAnchor( row, "topleft", F.name, "topleft", 5, (19 * num) - 19 + 14 )
    end
  end
  
  F.col1But = F("button", "scenarioInfoCol1Button", "DefaultButton")
  F.col1But:Position(165,5)
  WindowSetDimensions(F.col1But.name, 50,50)
  F.col1But.OnLButtonUp =
        function()
            ScenarioSummaryWindow.OnSortPlayerList()
            ScenarioSummaryWindow.OnSortPlayerList()
            ScenarioSummaryWindow.display.order = ScenarioSummaryWindow.SORT_ORDER_DOWN
            scenarioInfo.OnUpdate()            
        end
  F.col1But.OnRButtonUp =
        function()
            scenarioInfo.RightMouseColumn(1)      
        end	        
        
  F.col1 = F("image")
  F.col1:Position(165,5)
  F.col1:Resize(16,16)
  F.col1:Texture("EA_ScenarioSummary01_d5", 0, 0)
  DynamicImageSetTextureScale(F.col1.name, 0.5)

  F.col2But = F("button", "scenarioInfoCol2Button", "DefaultButton")
  F.col2But:Position(240,5)
  WindowSetDimensions(F.col2But.name, 50,50)
  F.col2But.OnLButtonUp =
        function()
            ScenarioSummaryWindow.OnSortPlayerList()
            ScenarioSummaryWindow.OnSortPlayerList()
            ScenarioSummaryWindow.display.order = ScenarioSummaryWindow.SORT_ORDER_DOWN
            scenarioInfo.OnUpdate()            
        end
  F.col2But.OnRButtonUp =
        function()
            scenarioInfo.RightMouseColumn(2)      
        end	         
  
  F.col2 = F("image")
  F.col2:Position(240,5)
  F.col2:Resize(14,14)
  F.col2:Texture("EA_ScenarioSummary01_d5", 0, 0)
  DynamicImageSetTextureScale(F.col2.name, 0.5)

  scenarioInfo.updateColumns()
  
  --[[   
    --/script scenarioInfo.PopulateFakeList()
    --/script WindowSetShowing("ScenarioSummaryWindow" , true)

  ]]--
  
  
  local scale = InterfaceCore.GetScale()
  F:Position(scenarioInfo.Settings.x/scale, scenarioInfo.Settings.y/scale)
  WindowSetScale(F.name,1)  
    
  F:Hide()
  
  RegisterEventHandler(SystemData.Events.SCENARIO_BEGIN, "scenarioInfo.Show")
  RegisterEventHandler(SystemData.Events.SCENARIO_END, "scenarioInfo.Hide")
  RegisterEventHandler(SystemData.Events.SCENARIO_PLAYERS_LIST_UPDATED, "scenarioInfo.OnUpdate")
  RegisterEventHandler(SystemData.Events.SCENARIO_PLAYERS_LIST_STATS_UPDATED, "scenarioInfo.OnUpdate") 
  RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "scenarioInfo.OnReload")

  
  LayoutEditor.RegisterWindow(F.name,L"scenarioInfo","",false,false,true)
  table.insert(LayoutEditor.EventHandlers, scenarioInfo.SavePosition)
  
  if ( GameData.Player.isInScenario ) then
    scenarioInfo.Show()
  end
end

function scenarioInfo.updateColumns()
    --col1
    col1 = scenarioInfo.columndefs[scenarioInfo.Settings.column1]
    F.col1But:SetId(col1.order)
    F.col1:Resize(col1.width, col1.height)
    if (ScenarioSummaryWindow.display.type == col1.order) then
        DynamicImageSetTextureSlice(F.col1.name, col1.slice .. "-highlighted")
    else
        DynamicImageSetTextureSlice(F.col1.name, col1.slice)
    end

    --col2
    col2 = scenarioInfo.columndefs[scenarioInfo.Settings.column2]
    F.col2But:SetId(col2.order)
    F.col2:Resize(col2.width, col2.height)
    if (ScenarioSummaryWindow.display.type == col2.order) then
        DynamicImageSetTextureSlice(F.col2.name, col2.slice .. "-highlighted")
    else
        DynamicImageSetTextureSlice(F.col2.name, col2.slice)
    end
end

function scenarioInfo.RightMouseColumn(column)
	EA_Window_ContextMenu.CreateContextMenu("scenarioInfoContext")
    for key,obj in pairs(scenarioInfo.columndefs) do
    
        local titleText = ScenarioSummaryWindow.sortData[obj.order].title
        local descText = ScenarioSummaryWindow.sortData[obj.order].desc
        gdata = EA_Window_ContextMenu.GameActionData( column, 0, towstring(key))
        EA_Window_ContextMenu.AddMenuItem( titleText .. L" - (" .. descText .. L")",  scenarioInfo.setColumn , false,  true, NULL, gdata)
    end
    
    EA_Window_ContextMenu.Finalize()
end

function scenarioInfo.setColumn()
    local actionType, actionId, actionText = WindowGetGameActionData(SystemData.ActiveWindow.name)

    if (actionType == 1) then
        scenarioInfo.Settings.column1 = tostring(actionText)
    end

    if (actionType == 2) then
        scenarioInfo.Settings.column2 = tostring(actionText)
    end    
    scenarioInfo.OnUpdate()   
end

function scenarioInfo.SavePosition()
    local x,y = WindowGetScreenPosition(F.name)
    scenarioInfo.Settings.x = x
    scenarioInfo.Settings.y = y
    scenarioInfo.Settings.scale = WindowGetScale(F.name)
  
    if ( not GameData.Player.isInScenario ) then
        scenarioInfo.Hide()
    end  
end

function scenarioInfo.Show()
    F:Show()
    DelayTime = 0
    RegisterEventHandler( SystemData.Events.UPDATE_PROCESSED, "scenarioInfo.Timer")
    WindowClearAnchors("scenarioInfo")
end

function scenarioInfo.OnReload()
    scenarioInfo.Show()
end

function scenarioInfo.Hide()
    UnregisterEventHandler( SystemData.Events.UPDATE_PROCESSED, "scenarioInfo.Timer")    
    F:Hide()
end

function scenarioInfo.Timer(elapsed)
	DelayTime = DelayTime - elapsed
    if DelayTime > 0 then
        return
    end
    DelayTime = REFRESH_DELAY
    
    if (LayoutEditor.isActive) then
        return
    end
    
    local scale = InterfaceCore.GetScale()
    F:Position(scenarioInfo.Settings.x/scale, scenarioInfo.Settings.y/scale)
    WindowSetScale(F.name,1)
    
    if ( not GameData.Player.isInScenario ) then
        scenarioInfo.Hide()
        return
    end
    
    if WindowGetShowing("ScenarioSummaryWindow") then
        DelayTime = REFRESH_DELAY_SCOREBOARD
    end
    
    BroadcastEvent( SystemData.Events.SCENARIO_START_UPDATING_PLAYERS_STATS)
    BroadcastEvent( SystemData.Events.SCENARIO_STOP_UPDATING_PLAYERS_STATS)    
end

function scenarioInfo.getDataForColumn(name, memberData)

   local column = scenarioInfo.columndefs[name]
   
   return memberData[column.datafield]
end

function scenarioInfo.OnUpdate()

  for num=1, scenarioInfo.maxRows do
        local row = "scenarioInfoPlayerRow"..num
        WindowSetShowing( row, false )
        scenarioInfo.updateColumns()
  end    
  
  num = 1
  if (ScenarioSummaryWindow.playerListOrder ~= nil) then	
      for key, dataIndex in ipairs( ScenarioSummaryWindow.playerListOrder ) do
          memberData = ScenarioSummaryWindow.playersData[ dataIndex ]
          local row = "scenarioInfoPlayerRow"..num
          num = num +1

          if (num > scenarioInfo.maxRows + 1) then
            return
          end
          local row_color
          local text_color = DefaultColor.WHITE	
          if( memberData.realm == GameData.Realm.ORDER ) then
            row_color = {r=12, g=47, b=158}
          else
            row_color = {r=158, g=12, b=13}
          end
      
        for key,name in pairs(scenarioInfo.allfields) do
            WindowSetTintColor(row.."Background"..name, row_color.r, row_color.g, row_color.b)
            WindowSetAlpha(row.."Background"..name, 0.2)
            LabelSetTextColor(row..name, text_color.r,text_color.g, text_color.b)    
            WindowSetShowing( row.."Background"..name , false )
            WindowSetDimensions(row..name, 0, 17)   
            WindowSetHandleInput(row..name, false)
        end
        
        --Career
        local texture, x, y = GetIconData(memberData.careerIcon)		
        DynamicImageSetTexture(row.."CareerIcon", texture, x, y)    
        WindowSetShowing( row.."BackgroundCareer" , true )
        WindowSetDimensions(row.."Career", 25, 17)    
        WindowSetDimensions(row.."CareerIcon", 18, 15)    
        DynamicImageSetTextureScale(row.."CareerIcon", 0.65)   
        
        --Name
        WindowSetShowing( row.."BackgroundName" , true )
        LabelSetText( row.."Name", memberData.name )
        LabelSetFont(row.."Name", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)        
        WindowSetDimensions(row.."Name", 100, 17)

        --Column1
        WindowSetShowing( row.."BackgroundDamageDealt" , true )
        LabelSetText( row.."DamageDealt", StringUtils.FormatNumberWString (scenarioInfo.getDataForColumn(scenarioInfo.Settings.column1, memberData)))
        LabelSetFont(row.."DamageDealt", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)        
        WindowSetDimensions(row.."DamageDealt", 75, 17)
     
        --Column2
        WindowSetShowing( row.."BackgroundHealingDealt" , true )
        LabelSetText( row.."HealingDealt", StringUtils.FormatNumberWString( scenarioInfo.getDataForColumn(scenarioInfo.Settings.column2, memberData) ))
        LabelSetFont(row.."HealingDealt", "font_clear_small", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)        
        WindowSetDimensions(row.."HealingDealt", 75, 17)

        WindowSetShowing ( row .."SelectionBorder", false)
        WindowSetShowing( row, true )
    end
  end
end
