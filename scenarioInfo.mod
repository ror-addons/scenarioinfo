<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="scenarioInfo" version="1.0" date="" >

		<Author name="smurfy" email="" />
		<Description text="simple always visible scenario status" />
        
        <VersionSettings gameVersion="1.3.3" windowsVersion="1.0" savedVariablesVersion="1.0" />
        <Dependencies>
            <Dependency name="EA_ScenarioSummaryWindow" />
        </Dependencies>
		<Files>
            <File name="LibStub.lua" />
            <File name="LibGUI.lua" />	 
            <File name="scenarioInfo.lua" />
		</Files>
		<SavedVariables>
			<SavedVariable name="scenarioInfo.Settings"/>
		</SavedVariables>		
		<OnInitialize>
            <CallFunction name="scenarioInfo.Initialize" />
		</OnInitialize>
		<OnShutdown/>
		
	</UiMod>
</ModuleFile>